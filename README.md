### ~~NPF images fix — v4.0~~&ensp;DISCONTINUED

<small>🗓️&ensp;Last updated: 2023/04/20 22:34 GMT-7</small>

An add-on for Tumblr by [glenthemes](//glenthemes.tumblr.com) that polishes any NPF photosets on your blog theme to make them appear more natural (mimicking what they're meant to look like on the dashboard), as the default styling for those images is unideal with most themes.

---

This version was unreleased and will be discontinued. Any blogs that are currently using this fix won't be affected, but if you're a new visitor, please use my v3 fix instead: [git.io/JRBt7](https://git.io/JRBt7)

---

Once again: this is out of service, please take your leave!
