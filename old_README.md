### NPF images fix — v4.0

<small>🗓️&ensp;Last updated: 2022/10/20 12:56 GMT-7</small>

An add-on for Tumblr by [glenthemes](//glenthemes.tumblr.com) that polishes any NPF photosets on your blog theme to make them appear more natural (mimicking what they're meant to look like on the dashboard), as the default styling for those images is unideal with most themes.

---

<small><b>HOUSEKEEPING:</b> (click to expand)</small>

<!------------->

<details>
  <summary>👈&ensp;<small><b>WHAT IS NPF?</b></small></summary>
  
  #### What is NPF?
  <blockquote>NPF stands for <b>Neue Post Format</b>. Tumblr used to have multiple types of post formats to choose from. In recent years they've started to turn many of those formats into NPF only (in other words, everything becomes a text post), which can be counterproductive to how the original poster wanted it to be displayed. <i>NPF images are different from regular photo posts</i> on Tumblr, so this fix only targets NPF images.</blockquote>

  #### How do you make NPF images?
  - all images and photosets uploaded via Tumblr mobile
  - by making posts with the [new post editor](https://rcserph.tumblr.com/post/642180220730966016)
  - any images included between paragraphs
</details>

<!-------------------------------------------------------->

<details>
  <summary>👈&ensp;<small><b>PREVIOUS VERSIONS</b></small></summary>
  <br>

  - [v1.0](https://glenthemes.tumblr.com/post/614223478203285504) &mdash; Apr 2020
  - [v2.0](https://glenthemes.tumblr.com/post/638038350689976320) &mdash; Dec 2020
  - [v3.0](https://glenthemes.tumblr.com/post/659034084446748672) &mdash; Aug 2021
</details>

<!-------------------------------------------------------->

<details>
  <summary>👈&ensp;<small><b>WARNING</b></small></summary>
  <br>

  If your theme already uses a NPF fix, such as my previous versions or @codematurgy's (search for `boscoxvi` in your theme), there will be conflicts and things may go wrong. If you _can_ find existing NPF modifications in your theme but you want to switch to this one, **you need to remove your existing NPF fixes.**

  I've tried to make this as easy to install as possible, but I can't guarantee that it'll be a "one size fits all" mod. Feel free to send me a message if you have any questions or problems.
</details>

<!-------------------------------------------------------->

<details>
  <summary>👈&ensp;<small><b>WHAT'S NEW</b></small></summary>
  <br>

  - can be used with or without jQuery
    <br>`$(document).ready(function(){...`
  - auto-adds jQuery if you don't already have it
  - more options for caption/photoset placement
  - lightboxes &mdash; works with mouse clicks & arrow keys
  - « <small>OPTIONAL</small> » change `.gifv` to `.gif`
</details>

---

#### Features:
- shows HD versions of images if available
- displays NPF images at their intended dimensions
- supports original blockquote captions as well as modern dashboard captions
- single and multi lightbox function
- custom images spacing
- custom spacing between images & captions

---

#### How to use:

1. Go into `Edit HTML` of your blog's customize page, and locate `{block:Posts` by typing it into the searchbar: 
![image](https://user-images.githubusercontent.com/110954255/184795109-403aa334-dfe7-4831-830f-a621723b3129.png)  
Highlight that line and replace it with:  
`{block:Posts inlineMediaWidth="1280" inlineNestedMediaWidth="1280"}`


2. We install the actual NPF mods in this step.  
You have two choices, you can either:  
❀  a. paste these just above `</head>`  
❀  b. paste these just above `</body>`
```html
<!------ NPF images fix (v4.0) by @glenthemes [2022] ------>
<!------         https://waa.ai/tmblr-npf-v4         ------>
<script src="//tmblr-npf-v4.github.io/npf-script.js"></script>
<link href="//tmblr-npf-v4.github.io/npf-styling.css" rel="stylesheet">
<link href="//assets.tumblr.com/client/prod/standalone/blog-network-npf/index.build.css" rel="stylesheet" media="screen">
<script>npf_v4_fix();</script>

<style>
:root {
    --NPF-GIFV-To-GIF:"yes";
    --NPF-Photoset-Spacing:4px;
    --NPF-Lightbox-Delay:100ms; /* can be ms or s */
    --NPF-Lightbox-FadeIn:100ms; /* can be ms or s */
    --NPF-Captions-Spacing:1em;

    --NPF-Move-1st-Photoset:"yes";
    --NPF-No-Caption-Remove-OP:"yes";
}
</style>
```
